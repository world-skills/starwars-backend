USE [master]
GO
/****** Object:  Database [StarWars]    Script Date: 5/21/2020 1:21:18 PM ******/
CREATE DATABASE [StarWars]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'StarWars', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\StarWars.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'StarWars_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\StarWars_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [StarWars] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [StarWars].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [StarWars] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [StarWars] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [StarWars] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [StarWars] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [StarWars] SET ARITHABORT OFF 
GO
ALTER DATABASE [StarWars] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [StarWars] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [StarWars] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [StarWars] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [StarWars] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [StarWars] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [StarWars] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [StarWars] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [StarWars] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [StarWars] SET  DISABLE_BROKER 
GO
ALTER DATABASE [StarWars] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [StarWars] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [StarWars] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [StarWars] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [StarWars] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [StarWars] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [StarWars] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [StarWars] SET RECOVERY FULL 
GO
ALTER DATABASE [StarWars] SET  MULTI_USER 
GO
ALTER DATABASE [StarWars] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [StarWars] SET DB_CHAINING OFF 
GO
ALTER DATABASE [StarWars] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [StarWars] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [StarWars] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'StarWars', N'ON'
GO
ALTER DATABASE [StarWars] SET QUERY_STORE = OFF
GO
USE [StarWars]
GO
/****** Object:  Table [dbo].[Force]    Script Date: 5/21/2020 1:21:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Force](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Guvernor]    Script Date: 5/21/2020 1:21:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guvernor](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[firstName] [nvarchar](50) NULL,
	[lastName] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Planet]    Script Date: 5/21/2020 1:21:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Planet](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Type] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[guvernorID] [int] NULL,
	[clan] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Planet_Force]    Script Date: 5/21/2020 1:21:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Planet_Force](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[planetID] [int] NULL,
	[forceID] [int] NULL,
	[size] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Force] ON 

INSERT [dbo].[Force] ([ID], [Name]) VALUES (1, N'Battle droid')
INSERT [dbo].[Force] ([ID], [Name]) VALUES (2, N'BX-Series Droid')
INSERT [dbo].[Force] ([ID], [Name]) VALUES (3, N'Driodeka')
INSERT [dbo].[Force] ([ID], [Name]) VALUES (4, N'Dwarf Spirder')
INSERT [dbo].[Force] ([ID], [Name]) VALUES (5, N'General Kalani')
INSERT [dbo].[Force] ([ID], [Name]) VALUES (6, N'Magna Guard')
INSERT [dbo].[Force] ([ID], [Name]) VALUES (7, N'Pollux Assasin')
INSERT [dbo].[Force] ([ID], [Name]) VALUES (8, N'Super battle droid')
INSERT [dbo].[Force] ([ID], [Name]) VALUES (9, N'Tactical droid')
SET IDENTITY_INSERT [dbo].[Force] OFF
SET IDENTITY_INSERT [dbo].[Guvernor] ON 

INSERT [dbo].[Guvernor] ([ID], [firstName], [lastName]) VALUES (1, N'Marin', N'Kenobi')
INSERT [dbo].[Guvernor] ([ID], [firstName], [lastName]) VALUES (2, N'Darth', N'Vader')
INSERT [dbo].[Guvernor] ([ID], [firstName], [lastName]) VALUES (3, N'Baby', N'Yoda')
INSERT [dbo].[Guvernor] ([ID], [firstName], [lastName]) VALUES (4, N'Luke', N'Skywalker')
INSERT [dbo].[Guvernor] ([ID], [firstName], [lastName]) VALUES (5, N'Chew', N'Baka')
SET IDENTITY_INSERT [dbo].[Guvernor] OFF
SET IDENTITY_INSERT [dbo].[Planet] ON 

INSERT [dbo].[Planet] ([ID], [Name], [Type], [Region], [guvernorID], [clan]) VALUES (1, N'Earth', N'Solid', N'Unknown Regions', 3, N'Galactic republic')
INSERT [dbo].[Planet] ([ID], [Name], [Type], [Region], [guvernorID], [clan]) VALUES (2, N'Venus', N'Gas Giant', N'The galaxy', 3, N'Galactic republic')
INSERT [dbo].[Planet] ([ID], [Name], [Type], [Region], [guvernorID], [clan]) VALUES (3, N'Mars', N'Gas Giant', N'Unknown Regions', 4, N'Confederation')
INSERT [dbo].[Planet] ([ID], [Name], [Type], [Region], [guvernorID], [clan]) VALUES (4, N'Pluto', N'Gas Giant', N'The galaxy', 5, N'Galactic republic')
INSERT [dbo].[Planet] ([ID], [Name], [Type], [Region], [guvernorID], [clan]) VALUES (5, N'Neptune', N'Gas Giant', N'The galaxy', 3, N'Confederation')
INSERT [dbo].[Planet] ([ID], [Name], [Type], [Region], [guvernorID], [clan]) VALUES (6, N'Abafar', N'Gas Giant', N'The galaxy', 3, N'Confederation')
INSERT [dbo].[Planet] ([ID], [Name], [Type], [Region], [guvernorID], [clan]) VALUES (7, N'Christophsis', N'Star', N'Outer Rim Territories', 4, N'Galactic republic')
INSERT [dbo].[Planet] ([ID], [Name], [Type], [Region], [guvernorID], [clan]) VALUES (8, N'Mimban', N'Gas Giant', N'The galaxy', 1, N'Galactic republic')
INSERT [dbo].[Planet] ([ID], [Name], [Type], [Region], [guvernorID], [clan]) VALUES (9, N'Mandalore', N'Gas Giant', N'The galaxy', 1, N'Galactic republic')
INSERT [dbo].[Planet] ([ID], [Name], [Type], [Region], [guvernorID], [clan]) VALUES (10, N'Subterrel', N'Water', N'Mid Rim', 1, N'Galactic republic')
INSERT [dbo].[Planet] ([ID], [Name], [Type], [Region], [guvernorID], [clan]) VALUES (11, N'Savareen', N'Gas Giant', N'The galaxy', 1, N'Galactic republic')
SET IDENTITY_INSERT [dbo].[Planet] OFF
SET IDENTITY_INSERT [dbo].[Planet_Force] ON 

INSERT [dbo].[Planet_Force] ([ID], [planetID], [forceID], [size]) VALUES (31, 1, 4, 666)
INSERT [dbo].[Planet_Force] ([ID], [planetID], [forceID], [size]) VALUES (32, 2, 3, 10)
INSERT [dbo].[Planet_Force] ([ID], [planetID], [forceID], [size]) VALUES (33, 2, 7, 10)
INSERT [dbo].[Planet_Force] ([ID], [planetID], [forceID], [size]) VALUES (34, 2, 1, 500)
INSERT [dbo].[Planet_Force] ([ID], [planetID], [forceID], [size]) VALUES (35, 7, 4, 1065)
INSERT [dbo].[Planet_Force] ([ID], [planetID], [forceID], [size]) VALUES (37, 7, 5, 1056)
INSERT [dbo].[Planet_Force] ([ID], [planetID], [forceID], [size]) VALUES (38, 1, 8, 10)
INSERT [dbo].[Planet_Force] ([ID], [planetID], [forceID], [size]) VALUES (39, 8, 4, 500)
INSERT [dbo].[Planet_Force] ([ID], [planetID], [forceID], [size]) VALUES (41, 8, 9, 10)
SET IDENTITY_INSERT [dbo].[Planet_Force] OFF
ALTER TABLE [dbo].[Planet_Force] ADD  DEFAULT ((0)) FOR [size]
GO
ALTER TABLE [dbo].[Planet]  WITH CHECK ADD FOREIGN KEY([guvernorID])
REFERENCES [dbo].[Guvernor] ([ID])
GO
ALTER TABLE [dbo].[Planet_Force]  WITH CHECK ADD FOREIGN KEY([forceID])
REFERENCES [dbo].[Force] ([ID])
GO
ALTER TABLE [dbo].[Planet_Force]  WITH CHECK ADD FOREIGN KEY([planetID])
REFERENCES [dbo].[Planet] ([ID])
GO
USE [master]
GO
ALTER DATABASE [StarWars] SET  READ_WRITE 
GO
