﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace StarWars_Backend.Controllers
{
    public class ForcesController : ApiController
    {
        private StarWarsEntities db = new StarWarsEntities();

        [HttpGet]
        [Route("forces/get-example-forces")]
        public IHttpActionResult GetExampleForces()
        {
            var forces = db.Forces.Select(it => new { it.ID, it.Name }).ToList();

            return Ok(forces);
        }

        [HttpPut]
        [Route("forces/add-force")]
        public IHttpActionResult AddForce([FromBody] Planet_Force body)
        {
            db.Planet_Force.Add(body);
            db.SaveChanges();

            var force = new { body.ID, body.planetID, body.forceID, body.size };

            return Ok(force);
        }

        [HttpPut]
        [Route("forces/update-force")]
        public IHttpActionResult UpdateForce([FromBody] Planet_Force body)
        {
            var force = db.Planet_Force.FirstOrDefault(it => it.ID == body.ID);
            force.size = body.size;
            db.SaveChanges();

            return Ok();
        }

        [HttpPut]
        [Route("forces/delete-force")]
        public IHttpActionResult DeleteForce(int id)
        {
            var force = db.Planet_Force.FirstOrDefault(it => it.ID == id);

            db.Planet_Force.Remove(force);

            db.SaveChanges();

            return Ok();
        }
    }
}
