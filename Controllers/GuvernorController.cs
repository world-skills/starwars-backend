﻿using StarWars_Backend.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Hosting;
using System.Web.Http;

namespace StarWars_Backend.Controllers
{
    public class GuvernorController : ApiController
    {
        private StarWarsEntities db = new StarWarsEntities();

        [HttpPut]
        [Route("guvernor/update-guvernor")]
        public IHttpActionResult UpdateGuvernor([FromBody] Guvernor body)
        {
            var guvernor = db.Guvernors.FirstOrDefault(it => it.ID == body.ID);

            guvernor.firstName = body.firstName;
            guvernor.lastName = body.lastName;

            db.SaveChanges();

            return Ok();
        }

        [HttpGet]
        [Route("guvernor/get-all-guvernors")]
        public IHttpActionResult GetAllGuvernors()
        {
            var guvenrors = db.Guvernors.Select(it =>
            new
            {
                it.ID,
                it.firstName,
                it.lastName
            }).ToList();

            return Ok(guvenrors);
        }


        [HttpGet]
        [Route("guvernor/get-guvernor-photo")]
        public HttpResponseMessage GetGuvernorImage([FromUri] int id)
        {
            String filePath = HostingEnvironment.MapPath($"~/Images/guvernors/{id}.jpg");

            if (!File.Exists(filePath))
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            var bytes = File.ReadAllBytes(filePath);

            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ByteArrayContent(bytes);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");


            return result;
        }


        [HttpPut]
        [Route("guvernor/upload-guvernor-photo")]
        public IHttpActionResult UploadGuvernorPhoto([FromBody] PhotoBody body, [FromUri] int id)
        {
            var bytes = Convert.FromBase64String(body.PhotoData);
            String filePath = HostingEnvironment.MapPath($"~/Images/guvernors/{id}.jpg");
            using (var imageFile = new FileStream(filePath, FileMode.Create))
            {
                imageFile.Write(bytes, 0, bytes.Length);
                imageFile.Flush();
            }

            return Ok();
        }
    }
}
