﻿using StarWars_Backend.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Hosting;
using System.Web.Http;

namespace StarWars_Backend.Controllers
{

    public class PlanetController : ApiController
    {
        private StarWarsEntities db = new StarWarsEntities();

        [HttpPut]
        [Route("planet/update-planet")]
        public IHttpActionResult UpdatePlanet([FromBody] Planet body)
        {
            var planet = db.Planets.FirstOrDefault(it => it.ID == body.ID);

            planet.Name = body.Name;
            planet.Type = body.Type;
            planet.clan = body.clan;
            planet.Region = body.Region;
            planet.guvernorID = body.guvernorID;

            db.SaveChanges();

            return Ok(planet);
        }


        [HttpGet]
        [Route("planet/get-all-planets")]
        public IHttpActionResult GetAllPlanets()
        {
            var planets = db.Planets.Select(it =>
            new {it.ID, it.Name, Guvernor = new { it.Guvernor.ID, it.Guvernor.firstName, it.Guvernor.lastName },
            it.Region,
            it.clan,
            it.Type, it.guvernorID,
            Forces = it.Planet_Force.Select(pf => new { pf.ID, pf.Force.Name, pf.forceID, pf.planetID, pf.size })});

            return Ok(planets);
        }

        [HttpPut]
        [Route("planet/upload-planet-photo")]
        public IHttpActionResult UploadPlanetPhoto([FromBody] PhotoBody body, [FromUri] int id)
        {
            var bytes = Convert.FromBase64String(body.PhotoData);
            String filePath = HostingEnvironment.MapPath($"~/Images/planets/{id}.jpg");
            using (var imageFile = new FileStream(filePath, FileMode.Create))
            {
                imageFile.Write(bytes,0,bytes.Length);
                imageFile.Flush();
            }

            return Ok();
        }

        [HttpGet]
        [Route("planet/get-planet-photo")]
        public HttpResponseMessage GetPlanetImage([FromUri] int id)
        {
            String filePath = HostingEnvironment.MapPath($"~/Images/planets/{id}.jpg");
            if (!File.Exists(filePath))
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            var bytes = File.ReadAllBytes(filePath);

            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ByteArrayContent(bytes);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");


            return result;
        }



    }
}
